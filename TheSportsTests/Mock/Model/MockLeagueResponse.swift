//
//  MockLeagueResponse.swift
//  TheSportsTests
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

struct MockLeagueResponse: LeagueResponse {
    var idLeague: String
    var strLeague: String
    var strSport: String
}
