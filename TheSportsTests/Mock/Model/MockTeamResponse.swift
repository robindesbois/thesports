//
//  MockTeamResponse.swift
//  TheSportsTests
//
//  Created by Regis Alla on 15/04/2024.
//

import Foundation

struct MockTeamResponse: TeamResponse {
    var idTeam: String
    var strTeam: String
    var strTeamBadge: String
}
