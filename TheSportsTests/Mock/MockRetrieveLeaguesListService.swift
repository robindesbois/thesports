//
//  MockRetrieveLeaguesListService.swift
//  TheSportsTests
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

struct MockRetrieveLeaguesListService: RetrieveLeaguesListServiceProtocol {
    func getLeaguesList() -> AnyPublisher<[LeagueResponse], NetworkError> {
        let mockData: [LeagueResponse] = [
            MockLeagueResponse(idLeague: "1", strLeague: "Premier League", strSport: "Soccer"),
            MockLeagueResponse(idLeague: "2", strLeague: "La Liga", strSport: "Soccer"),
            MockLeagueResponse(idLeague: "3", strLeague: "Ligue 1", strSport: "Basketball")
        ]
        
        return Just(mockData)
            .setFailureType(to: NetworkError.self)
            .eraseToAnyPublisher()
    }
}

