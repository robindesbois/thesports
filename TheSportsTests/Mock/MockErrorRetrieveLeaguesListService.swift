//
//  MockErrorRetrieveLeaguesListService.swift
//  TheSportsTests
//
//  Created by Regis Alla on 14/04/2024.
//

import Combine
import Foundation

struct MockErrorRetrieveLeaguesListService: RetrieveLeaguesListServiceProtocol {
    
    func getLeaguesList() -> AnyPublisher<[LeagueResponse], NetworkError> {
        let error = URLError(.cannotConnectToHost)
        
        return Fail<[LeagueResponse], NetworkError>(error: .networkError(error)).eraseToAnyPublisher()
    }
}
