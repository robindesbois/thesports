//
//  MockRetrieveBadgeTeamService.swift
//  TheSports
//
//  Created by Regis Alla on 15/04/2024.
//

import Foundation
import Combine
import SwiftUI

class MockRetrieveBadgeTeamService: RetrieveTeamBadgeServiceProtocol {
    
    func getBadgeTeam(_ urlBadge: String) -> AnyPublisher<Data, NetworkError> {
        guard let systemImage = UIImage(systemName: "house.fill") else {
            return Fail<Data, NetworkError>(error: .invalidURL).eraseToAnyPublisher()
        }
        
        guard let imageData = systemImage.pngData() else {
            return Fail<Data, NetworkError>(error: .invalidURL).eraseToAnyPublisher()
        }
        
        let mockedImageData = Data(imageData)
        
        return Just(mockedImageData)
            .setFailureType(to: NetworkError.self)
            .eraseToAnyPublisher()
    }
}
