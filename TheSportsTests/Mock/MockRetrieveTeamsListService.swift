//
//  MockRetrieveTeamsListService.swift
//  TheSportsTests
//
//  Created by Regis Alla on 15/04/2024.
//

import Foundation
import Combine

class MockRetrieveTeamsListService: RetrieveTeamsListServiceProtocol {
    
    func getTeamsList(_ leagueName: String) -> AnyPublisher<[TeamResponse], NetworkError> {
        // Stactic Value
        let team1 = MockTeamResponse(idTeam: "1", strTeam: "Marseille", strTeamBadge: "badge_url_1")
        let team2 = MockTeamResponse(idTeam: "2", strTeam: "Paris", strTeamBadge: "badge_url_2")
        let team3 = MockTeamResponse(idTeam: "3", strTeam: "Lens", strTeamBadge: "badge_url_3")
        let team4 = MockTeamResponse(idTeam: "4", strTeam: "Caen", strTeamBadge: "badge_url_4")
        let team5 = MockTeamResponse(idTeam: "5", strTeam: "Lyon", strTeamBadge: "badge_url_5")
        let team6 = MockTeamResponse(idTeam: "6", strTeam: "Strasbourg", strTeamBadge: "badge_url_6")
        let team7 = MockTeamResponse(idTeam: "7", strTeam: "Nice", strTeamBadge: "badge_url_7")
        let team8 = MockTeamResponse(idTeam: "8", strTeam: "Le Mans", strTeamBadge: "badge_url_8")
        let team9 = MockTeamResponse(idTeam: "9", strTeam: "Lille", strTeamBadge: "badge_url_9")
        let teams = [team1, team2, team3, team4, team5, team6, team7, team8, team9]
        
        return Just(teams)
            .setFailureType(to: NetworkError.self)
            .eraseToAnyPublisher()
    }
}
