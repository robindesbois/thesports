//
//  TeamsViewModelTests.swift
//  TheSports
//
//  Created by Regis Alla on 15/04/2024.
//

import XCTest
import Combine
@testable import TheSports

class TeamsViewModelTests: XCTestCase {
    var cancellables = Set<AnyCancellable>()
    var viewModel: TeamsViewModel!
    var mockInteractor: TeamsInteractor!
    
    var mockTeams: [Team] {
        let team1 = Team(id: "6", name: "Strasbourg", teamBadge: "badge_url_6")
        let team2 = Team(id: "7", name: "Nice", teamBadge: "badge_url_7")
        let team3 = Team(id: "5", name: "Lyon", teamBadge: "badge_url_5")
        let team4 = Team(id: "3", name: "Lens", teamBadge: "badge_url_3")
        let team5 = Team(id: "4", name: "Caen", teamBadge: "badge_url_4")
        return [team1, team2, team3, team4, team5]
    }
    
    override func setUp() {
        super.setUp()
        let mockTeamListService = MockRetrieveTeamsListService()
        let mockBadgeService = MockRetrieveBadgeTeamService()
        
        let league = League(id: "1", name: "Premier League")
        mockInteractor = TeamsInteractor(serviceRetrieveTeams: mockTeamListService, serviceRetrieveBadgeTeams: mockBadgeService)
        viewModel = TeamsViewModel(league, interactor: mockInteractor)
    }
    
    override func tearDown() {
        viewModel = nil
        mockInteractor = nil
        super.tearDown()
    }
    
    func testSortedTeams() {
        // Given
        let expectation = XCTestExpectation(description: "Teams should be loaded")
        
        viewModel.$teams
            .dropFirst()
            .sink { _ in
                expectation.fulfill()
            }
            .store(in: &cancellables)
        
        // When
        viewModel.load()
        wait(for: [expectation], timeout: 1.0)
        
        // Then
        XCTAssertEqual(viewModel.teams, mockTeams)
    }
}
