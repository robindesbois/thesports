//
//  LeaguesViewModelTests.swift
//  TheSportsTests
//
//  Created by Regis Alla on 14/04/2024.
//

import XCTest
import Combine
@testable import TheSports

class LeaguesViewModelTests: XCTestCase {
    var cancellables = Set<AnyCancellable>()
    var viewModel: LeaguesViewModel!
    var mockInteractor: LeaguesInteractor!
    
    override func setUp() {
        super.setUp()
        mockInteractor = LeaguesInteractor(service: MockRetrieveLeaguesListService())
        viewModel = LeaguesViewModel(interactor: mockInteractor)
    }
    
    override func tearDown() {
        viewModel = nil
        mockInteractor = nil
        super.tearDown()
    }
    
    func testFilteredLeaguesUpdates() {
        // Given
        viewModel.searchText = "Premier"
        let expectation = XCTestExpectation(description: "Filtered leagues should be updated")
        
        viewModel.$filteredLeagues
            .dropFirst()
            .sink { _ in
                expectation.fulfill()
            }
            .store(in: &cancellables)
        
        // When
        viewModel.load()
        wait(for: [expectation], timeout: 3.0)
        
        // Then
        XCTAssertEqual(viewModel.filteredLeagues.count, 1)
        XCTAssertEqual(viewModel.filteredLeagues[0].name, "Premier League")
    }
    
    func testLoadLeagues() {
        // Given
        let expectation = XCTestExpectation(description: "Filtered leagues should be updated")
        
        viewModel.$filteredLeagues
            .dropFirst()
            .sink { _ in
                expectation.fulfill()
            }
            .store(in: &cancellables)
        
        // When
        viewModel.load()
        
        // Then
        XCTAssertEqual(self.viewModel.filteredLeagues.count, 0)
    }
    
    func testErrorHandling() {
        // Given
        let expectation = XCTestExpectation(description: "Error should be handled")
        
        let errorInteractor = LeaguesInteractor(service: MockErrorRetrieveLeaguesListService())
        viewModel = LeaguesViewModel(interactor: errorInteractor)
        
        viewModel.$filteredLeagues
            .sink { _ in
                expectation.fulfill()
            }
            .store(in: &cancellables)
        
        // When
        viewModel.load()
        
        // Then
        XCTAssertEqual(self.viewModel.filteredLeagues.count, 0)
    }
    
    func testMessageError() {
        // Given
        let expectation = XCTestExpectation(description: "Error should be handled")
        
        let errorInteractor = LeaguesInteractor(service: MockErrorRetrieveLeaguesListService())
        viewModel = LeaguesViewModel(interactor: errorInteractor)
        
        viewModel.$errorMessage
            .dropFirst() // Ignorer le premier event qui est vide lors de l'initialisation
            .sink { _ in
                expectation.fulfill()
            }
            .store(in: &cancellables)
        
        // When
        viewModel.load()
        wait(for: [expectation], timeout: 1)

        // Then
        XCTAssertEqual(viewModel.errorMessage, "serverNotConnect")
    }
}
