//
//  RetrieveLeaguesList.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

struct RetrieveLeaguesList {
    private let service: RetrieveLeaguesListServiceProtocol
    
    init(service: RetrieveLeaguesListServiceProtocol ) {
        self.service = service
    }
    
    func executer() -> AnyPublisher<[LeagueResponse], NetworkError> {
        return service.getLeaguesList()
    }
}
