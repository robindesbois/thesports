//
//  RetrieveTeamsList.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

struct RetrieveTeamsList {
    private let service: RetrieveTeamsListServiceProtocol
    
    init(service: RetrieveTeamsListServiceProtocol) {
        self.service = service
    }
    
    func executer(_ leagueName: String) -> AnyPublisher<[TeamResponse], NetworkError> {
        return service.getTeamsList(leagueName)
            .map { teams in
                sortTeamsAndFilter(teams)
            }
            .eraseToAnyPublisher()
    }
    
    private func sortTeamsAndFilter(_ teams: [TeamResponse]) -> [TeamResponse] {
        // Sort anti-alphabetically
        let sortedTeams = teams.sorted { $0.strTeam > $1.strTeam }
        
        // Only return one team out of two
        let filteredTeams = sortedTeams.enumerated().compactMap { (index, team) in
            index % 2 == 0 ? team : nil
        }
        return filteredTeams
    }
}
