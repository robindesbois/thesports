//
//  LeagueResponse.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

protocol LeagueResponse {
    var idLeague: String { get }
    var strLeague: String { get }
    var strSport: String { get }
}
