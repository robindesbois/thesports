//
//  TeamResponse.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

protocol TeamResponse {
    var idTeam: String { get }
    var strTeam: String { get }
    var strTeamBadge: String { get }
}
