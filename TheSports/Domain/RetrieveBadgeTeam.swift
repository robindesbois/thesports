//
//  RetrieveBadgeTeam.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

struct RetrieveBadgeTeam {
    private let service: RetrieveTeamBadgeServiceProtocol
    
    init(service: RetrieveTeamBadgeServiceProtocol) {
        self.service = service
    }
    
    func executer(_ urlBadge: String) -> AnyPublisher<Data, NetworkError> {
        return service.getBadgeTeam(urlBadge)
    }
}
