//
//  TheSportsApp.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import SwiftUI

@main
struct TheSportsApp: App {
    var body: some Scene {
        WindowGroup {
            LeaguesConfigurator.configureListeLigueView()
        }
    }
}
