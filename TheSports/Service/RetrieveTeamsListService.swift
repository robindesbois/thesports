//
//  RetrieveTeamsListService.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

final class RetrieveTeamsListService: RetrieveTeamsListServiceProtocol {
    private let networker: NetworkerProtocol
    
    init(networker: NetworkerProtocol = Networker()) {
        self.networker = networker
    }
    
    func getTeamsList(_ leagueName: String) -> AnyPublisher<[TeamResponse], NetworkError> {
        let endpoint = Endpoint.teamList(with: leagueName)
        
        return networker.get(type: TeamsListResponseModel.self,
                             url: endpoint.url)
        .map { $0.teams }
        .eraseToAnyPublisher()
    }
}
