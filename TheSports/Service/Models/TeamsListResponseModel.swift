//
//  TeamsListResponseModel.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

struct TeamsListResponseModel: Decodable {
    var teams: [TeamResponseModel]
}
