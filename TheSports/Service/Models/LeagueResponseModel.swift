//
//  LeagueResponseModel.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

struct LeagueResponseModel: Decodable, LeagueResponse {
    let idLeague: String
    let strLeague: String
    let strSport: String
}
