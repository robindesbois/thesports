//
//  TeamResponseModel.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

struct TeamResponseModel: Decodable, TeamResponse {
    let idTeam: String
    let strTeam: String
    let strTeamBadge: String
}
