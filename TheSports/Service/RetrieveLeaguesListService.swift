//
//  RetrieveLeaguesListService.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

final class RetrieveLeaguesListService: RetrieveLeaguesListServiceProtocol {
    private let networker: NetworkerProtocol
    
    init(networker: NetworkerProtocol = Networker()) {
        self.networker = networker
    }
    
    func getLeaguesList() -> AnyPublisher<[LeagueResponse], NetworkError> {
        let endpoint = Endpoint.allLigues
        
        return networker.get(type: LeaguesListResponseModel.self,
                             url: endpoint.url).map { $0.leagues }
            .eraseToAnyPublisher()
    }
}
