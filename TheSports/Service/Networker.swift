//
//  Networker.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

class Networker: NetworkerProtocol {
    
    func get<T>(type: T.Type, url: URL) -> AnyPublisher<T, NetworkError> where T: Decodable {
        guard url.scheme != nil && url.host != nil else {
            return Fail<T, NetworkError>(error: .invalidURL).eraseToAnyPublisher()
        }
        
        let urlRequest = URLRequest(url: url)
        
        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .tryMap { data, response in
                guard let httpResponse = response as? HTTPURLResponse else {
                    throw NetworkError.networkError(URLError(.badServerResponse))
                }
                
                guard (200...299).contains(httpResponse.statusCode) else {
                    throw NetworkError.networkError(URLError(.badServerResponse))
                }
                
                return data
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { error in
                if let urlError = error as? URLError {
                    return .networkError(urlError)
                } else {
                    return .networkError(URLError(.unknown))
                }
            }
            .eraseToAnyPublisher()
    }
    
    func getData(url: URL) -> AnyPublisher<Data, NetworkError> {
        guard url.scheme != nil && url.host != nil else {
            return Fail<Data, NetworkError>(error: .invalidURL).eraseToAnyPublisher()
        }
        
        let urlRequest = URLRequest(url: url)
        
        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .tryMap { data, response in
                guard let httpResponse = response as? HTTPURLResponse else {
                    throw NetworkError.networkError(URLError(.badServerResponse))
                }
                
                guard (200...299).contains(httpResponse.statusCode) else {
                    throw NetworkError.networkError(URLError(.badServerResponse))
                }
                
                return data
            }
            .mapError { error in
                if let urlError = error as? URLError {
                    return .networkError(urlError)
                } else {
                    return .networkError(URLError(.unknown))
                }
            }
            .eraseToAnyPublisher()
    }
}
