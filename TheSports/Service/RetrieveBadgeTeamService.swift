//
//  RetrieveBadgeTeamService.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

final class RetrieveBadgeTeamService: RetrieveTeamBadgeServiceProtocol {
    internal let networker: NetworkerProtocol
    
    init(networker: NetworkerProtocol = Networker()) {
        self.networker = networker
    }
    
    func getBadgeTeam(_ url: String) -> AnyPublisher<Data, NetworkError> {
        guard let url = URL(string: url) else {
            return Fail<Data, NetworkError>(error: .invalidURL).eraseToAnyPublisher()
        }
        
        return networker.getData(url: url)
            .eraseToAnyPublisher()
    }
}
