//
//  RetrieveTeamsListServiceProtocol.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

protocol RetrieveTeamsListServiceProtocol {
    func getTeamsList(_ leagueName: String) -> AnyPublisher<[TeamResponse], NetworkError>
}
