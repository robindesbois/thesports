//
//  NetworkerProtocol.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

protocol NetworkerProtocol {
    func get<T>(type: T.Type, url: URL) -> AnyPublisher<T, NetworkError> where T: Decodable
    func getData(url: URL) -> AnyPublisher<Data, NetworkError>
}

enum NetworkError: Error {
    case invalidURL
    case networkError(URLError)
    
    var localizedDescription: String {
        switch self {
        case .invalidURL:
            return NSLocalizedString("urlNotValid", comment: "")
        case .networkError(let urlError):
            switch urlError.code {
            case .notConnectedToInternet:
                return NSLocalizedString("internetInactive", comment: "")
            case .cannotConnectToHost:
                return NSLocalizedString("serverNotConnect", comment: "")
            case .timedOut:
                return NSLocalizedString("requestTimeOut", comment: "")
            default:
                return NSLocalizedString("networkErrorUnknown", comment: "")
            }
        }
    }
}
