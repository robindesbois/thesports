//
//  RetrieveTeamsBadgeServiceProtocol.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

protocol RetrieveTeamsBadgeServiceProtocol {    
    func getBadgeTeam(_ url: String) -> AnyPublisher<Data, NetworkError>
}
