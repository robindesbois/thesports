//
//  LeaguesRouter.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import SwiftUI

final class LeaguesRouter {
    public static func destinationForTappedLigue(ligue: League) -> some View {
        return TeamsConfigurator.configureTeamsView(with: ligue)
    }
}
