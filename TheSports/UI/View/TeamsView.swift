//
//  TeamsView.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import SwiftUI
import Combine

struct TeamsView: View {
    @ObservedObject var viewModel: TeamsViewModel
    @State private var showingAlert = false
    
    var body: some View {
        NavigationView {
            scrollView
                .onAppear(perform: viewModel.load)
                .onChange(of: viewModel.errorMessage) { oldMessage, newMessage in
                    showingAlert = !newMessage.isEmpty ? true : false
                }
                .alert(isPresented: $showingAlert) {
                    Alert(
                        title: Text(NSLocalizedString("error", comment: "")),
                        message: Text(viewModel.errorMessage),
                        dismissButton: .default(Text(NSLocalizedString("ok", comment: ""))) {
                            showingAlert = false
                        }
                    )
                }
        }
    }
    
    var scrollView: some View {
        ScrollView {
            LazyVGrid(columns: columns, spacing: 20) {
                content
            }
            .padding(.horizontal)
        }
    }
    
    var content: some View {
        ForEach(viewModel.teams.indices, id: \.self) { index in
            badge(forIndex: index)
                .frame(width: 100, height: 100)
        }
    }
    
    func badge(forIndex index: Int) -> some View {
        Group {
            if let badgeImage = viewModel.badgeImage(at: index) {
                Image(uiImage: badgeImage)
                    .resizable()
            } else {
                ProgressView()
            }
        }
    }
    
    let columns = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
}
