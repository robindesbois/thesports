//
//  LeaguesView.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import SwiftUI

struct LeaguesView: View {
    @ObservedObject var viewModel: LeaguesViewModel
    @State private var showingAlert = false
    
    var body: some View {
        NavigationStack {
            suggestionsList
        }
        .searchable(text: $viewModel.searchText)
        .onAppear(perform: viewModel.load)
        .onChange(of: viewModel.errorMessage) { oldMessage, newMessage in
            showingAlert = !newMessage.isEmpty ? true : false
        }
        .alert(isPresented: $showingAlert) {
            Alert(
                title: Text(NSLocalizedString("error", comment: "")),
                message: Text(viewModel.errorMessage),
                dismissButton: .default(Text(NSLocalizedString("ok", comment: ""))) {
                    showingAlert = false
                }
            )
        }
    }
    
    var suggestionsList: some View {
        List(viewModel.filteredLeagues) { ligue in
            NavigationLink(destination: LeaguesRouter.destinationForTappedLigue(ligue: ligue)) {
                Text(ligue.name)
            }
        }
    }
}
