//
//  TeamsInteractor.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

struct TeamsInteractor {
    private let retrieveTeamsList: RetrieveTeamsList
    private let retrieveBadgeTeam: RetrieveBadgeTeam
    
    init(serviceRetrieveTeams: RetrieveTeamsListServiceProtocol,
         serviceRetrieveBadgeTeams: RetrieveTeamBadgeServiceProtocol) {
        self.retrieveTeamsList = RetrieveTeamsList(service: serviceRetrieveTeams)
        self.retrieveBadgeTeam = RetrieveBadgeTeam(service: serviceRetrieveBadgeTeams)
    }
    
    func retrieveListOfTeams(with leagueName: String) -> AnyPublisher<[Team], NetworkError>{
        return retrieveTeamsList.executer(leagueName)
            .map { $0.map {
                Team.from($0) }
            }
            .eraseToAnyPublisher()
    }
    
    func retrieveBadgeOfTeam(_ url: String) -> AnyPublisher<Data, NetworkError>{
        return retrieveBadgeTeam.executer(url)
            .eraseToAnyPublisher()
    }
}
