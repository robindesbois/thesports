//
//  LeaguesInteractor.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

struct LeaguesInteractor {
    private let service: RetrieveLeaguesListServiceProtocol
    
    init(service: RetrieveLeaguesListServiceProtocol) {
        self.service = service
    }
    
    func retrieveListOfLeagues() -> AnyPublisher<[League], NetworkError> {
        return service.getLeaguesList()
            .map { $0.map { League.from($0) } }
            .eraseToAnyPublisher()
    }
}
