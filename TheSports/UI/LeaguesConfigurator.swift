//
//  LeaguesConfigurator.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

final class LeaguesConfigurator {
    public static func configureListeLigueView(with viewModel: LeaguesViewModel = LeaguesViewModel(interactor: LeaguesInteractor(service: RetrieveLeaguesListService()))) -> LeaguesView {
        let leaguesView = LeaguesView(viewModel: viewModel)
        return leaguesView
    }
}
