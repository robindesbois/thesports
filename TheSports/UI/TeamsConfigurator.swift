//
//  TeamsConfigurator.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import SwiftUI

final class TeamsConfigurator {
    
    public static func configureTeamsView(with leagues: League) -> TeamsView {
        let interactor = TeamsInteractor(serviceRetrieveTeams: RetrieveTeamsListService(),
                                         serviceRetrieveBadgeTeams: RetrieveBadgeTeamService())
        let teamsView = TeamsView(viewModel: TeamsViewModel(leagues,
                                                            interactor: interactor))
        return teamsView
    }
}

