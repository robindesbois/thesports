//
//  League.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

struct League: Identifiable {
    let id: String
    let name: String
    
    static func from(_ ligueReponse: LeagueResponse) -> Self{
        Self(id: ligueReponse.idLeague, name: ligueReponse.strLeague)
    }
}
