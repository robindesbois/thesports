//
//  Team.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

struct Team: Identifiable, Equatable {
    var id: String
    let name: String
    var teamBadge : String
    
    static func from(_ equipeReponse: TeamResponse) -> Self{
        Self(id: equipeReponse.idTeam,
             name: equipeReponse.strTeam,
             teamBadge: equipeReponse.strTeamBadge)
    }
}
