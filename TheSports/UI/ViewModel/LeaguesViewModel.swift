//
//  LeaguesViewModel.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine

class LeaguesViewModel: ObservableObject {
    @Published private(set) var filteredLeagues: [League] = []
    @Published private(set) var errorMessage: String = ""
    @Published var searchText: String = "" {
        didSet {
            updateFilteredLeagues()
        }
    }
    
    private var leagues: [League] = []
    private var interactor: LeaguesInteractor
    private var cancellables = Set<AnyCancellable>()
    
    init(interactor: LeaguesInteractor) {
        self.interactor = interactor
    }
    
    func load() {
        retrieveListOfLeagues()
    }
    
    private func retrieveListOfLeagues() {
        interactor.retrieveListOfLeagues()
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                case .finished:
                    break
                }
            } receiveValue: { [weak self] leagues in
                guard let self = self else { return }
                self.leagues = leagues
                self.updateFilteredLeagues()
            }
            .store(in: &cancellables)
    }
    
    private func updateFilteredLeagues() {
        if searchText.isEmpty {
            self.filteredLeagues = []
        } else {
            self.filteredLeagues = self.leagues.filter { $0.name.localizedCaseInsensitiveContains(searchText) }
        }
    }
}
