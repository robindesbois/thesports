//
//  TeamsViewModel.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation
import Combine
import UIKit

class TeamsViewModel: ObservableObject {
    @Published private(set) var badgeListe: [UIImage] = []
    @Published private(set) var teams: [Team] = []
    @Published var errorMessage: String = ""
    
    private var league: League
    private var interactor: TeamsInteractor
    private var imageCache = NSCache<NSString, UIImage>()
    
    private var cancellables = Set<AnyCancellable>()
    
    init(_ league: League, interactor: TeamsInteractor) {
        self.league = league
        self.interactor = interactor
    }
    
    func load() {
        retrieveListOfTeams(with: league)
    }
    
    private func retrieveListOfTeams(with league: League) {
        interactor.retrieveListOfTeams(with: league.name)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                case .finished: break
                }
            } receiveValue: { [weak self] valeur in
                guard let self = self else { return }
                self.teams = valeur
                self.badgeListe = Array(repeating: UIImage(), count: valeur.count)
                self.retrieveTeamBadges()
            }
            .store(in: &cancellables)
    }
    
    private func retrieveTeamBadges() {
        for team in teams {
            retrieveBadgeOfTeam(for: team)
        }
    }
    
    private func retrieveBadgeOfTeam(for team: Team) {
        let imageUrlString = team.teamBadge
        
        if let cachedImage = imageCache.object(forKey: imageUrlString as NSString) {
            // Find team index in sorted array
            if let index = self.teams.firstIndex(where: { $0.teamBadge == imageUrlString }) {
                self.badgeListe[index] = cachedImage
            }
            return
        }
        
        interactor.retrieveBadgeOfTeam(imageUrlString)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let error):
                    print(error)
                case .finished: break
                }
            } receiveValue: { [weak self] data in
                guard let self = self, let image = UIImage(data: data) else { return }
                
                self.imageCache.setObject(image, forKey: imageUrlString as NSString)
                if let index = self.teams.firstIndex(where: { $0.teamBadge == imageUrlString }) {
                    self.badgeListe[index] = image
                }
            }
            .store(in: &cancellables)
    }
    
    func badgeImage(at index: Int) -> UIImage? {
        guard badgeListe.indices.contains(index) else {
            return nil
        }
        return badgeListe[index]
    }
}
