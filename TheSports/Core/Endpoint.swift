//
//  Endpoint.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

struct Endpoint {
    var path: String
    var queryItems: [URLQueryItem] = []
    var apiKey: String {
        "50130162"
    }
}
