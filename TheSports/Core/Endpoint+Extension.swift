//
//  Endpoint+Extension.swift
//  TheSports
//
//  Created by Regis Alla on 14/04/2024.
//

import Foundation

extension Endpoint {
    var url: URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "www.thesportsdb.com"
        components.path = "/api/v1/json/" + apiKey + path
        components.queryItems = queryItems
        
        guard let url = components.url else {
            preconditionFailure("Invalid URL components: \(components)")
        }
        return url
    }
    
    static var allLigues: Self {
        return Endpoint(path: "/all_leagues.php")
    }
    
    static func teamList(with leagueName: String) -> Self {
        return Endpoint(path: "/search_all_teams.php", queryItems: [
            URLQueryItem(name: "l", value: "\(leagueName)")
        ])
    }
}
